Changelog
===============================================================================

Notable changes between releases. Versions in (brackets) are being proposed and
not yet published.

0.0.3
-------------------------------------------------------------------------------

Complete overhaul of the conditor system.

0.0.2
-------------------------------------------------------------------------------

Coniguration system overhaul.

Persistent local configuration.

Actions as build recipe stages.

0.0.1
-------------------------------------------------------------------------------

Initial in development Release.

