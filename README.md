Conditor
===============================================================================

Project management, configuration and build utility.

Features
-------------------------------------------------------------------------------

### Configuration

Update the project version number at only one place and keep it consistent across new builds.

Define configuration globally at the project root or lacally in a specific directory.

### Composer

Used to copy a file tree and specify special formating or handling of files.

The SFS (special file suffix) of the Conditor composer allows to define custom actions during cloning of a file tree.

Conditor provides a context object, which can be used in local scripts to access the parent project and local / ongoing build instance.

### Actions

Define functions to execute while formating a string or add a new page to a documentation with a custom project action.

### Recipes

Streamline a build process using a recipe.

Add flavour values, to customize a streamlined build process.

### Build

Build processes, can be accessed as an object in your Python script.

Installing
-------------------------------------------------------------------------------

Install conditor using PyPI.

```
pip3 install conditor
```

Links
-------------------------------------------------------------------------------

GitLab repository

PyPI package

RTD documentation

