Development
===============================================================================

Notes for ongoing development.

Current
-------------------------------------------------------------------------------

### docs_baker

include prolog and epilog

build sub-document

Suggestions
-------------------------------------------------------------------------------

Read The Doc recipe.

Python Package direct PyPI upload.

TODO
-------------------------------------------------------------------------------

### Imediate

### Soon

Allow adding pathlib paths to build data without casting manually (JSON serialization error).
Separate packages for extensions.

### Future

Build test.

### Others

Included from sphinx autodoc:

