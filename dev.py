#!/usr/bin/env -S PYTHONPATH=./src python3 -B



import pathlib

import conditor
import conditor.project
import conditor.config




project = conditor.ctx['__PROJECT__']

#project.action.import_action('conditor.util.actions')
#conditor.ctx['A']['conditor.util.actions/TestAction']()

# Build docs
devdocs = project.recipe['docs'].default.new_build('DEVDOC')
devdocs.stage_all()



exit()


# L = locals     = L['path/src/']['local.cfg']
# G = globals    = G['name']
# B = build      = B['build-id']['stage']
# T = this-build = T['stage']
# Q = Recipe     = Q['package-build'].make()
# I = Internal   = I['datetime']
# A = Action     = A['my/action'](param)


def lce(cfgmgr, values=False) :
    l = []
    for name in cfgmgr.list_entry_names() :
        e = f'{name}'
        if values :
            e += f' = {cfgmgr[name]}'
            pass
        l.append(e)
        pass
    return l


print('-'*80)
project = conditor.project.find_project(__file__)
print('Project:', project)
print('Global Config Mgr:', project.global_config)
print('Local Config Mgr:', project.local_config)
#print('Global entry Names', project.global_config.list_entry_names())

project.recipe.import_recipe('conditor.docs_baker.recipe')
project.recipe.import_recipe('conditor.sphinx.recipe')
project.recipe.import_recipe('conditor.py3pkg.recipe')

print('-'*80)
print('- Config')

print('-'*40)
print('-- Globals')
gcfg = project.global_config
#G['project.title'] = 'Conditor'
#G['project.codename'] = 'conditor'
#G['project.authors'] = [('srh', 'Sophia Roger Hürzeler', 'srh@srhuerzeler.net')]
#G['project.license.tag'] = 'MIT'
#G['project.description'] = 'Description Text.'
#version = (conditor.config.FormatString, '{G["project.version.major"]}.{G["project.version.minor"]}.{G["project.version.patch"]}')
#G['project.version'] = version
#print(G['project.version'])
#G['project.version.major'] = 0
#G['project.version.minor'] = 0
#G['project.version.patch'] = 0
#print('\n'.join(gcfg.list_entry_names()))
print(' '+'\n '.join(lce(gcfg, True)))

print('-'*80)





#import conditor.docs_baker
#doc = conditor.docs_baker.Document('./docs', 'my_document')
#composer = conditor.docs_baker.Composer(doc, pathlib.Path('./docout').resolve())
#composer.struct = {
#        '/': {'content': 'testing content'},
#        'my_document/pypkg': {'pages': [[True, 'api']]}
#}
#composer.compose()




#composer.compose_struct(doc_struct)
#composer.compose()

#print(doc.list_all_fragments())
#print(composer.handle(doc['index']['content']))
#print(composer.compose_fragment(doc['index'], 1))


print('-'*80)
print('- Building docs')
docs_build = project.recipe['docs']['HTML'].new_build('devdocs')
print(docs_build)
docs_build.stage_all()
print('-'*80)






exit()

print('-'*80)
print('- Building docs')
#docs_build = project.recipe['docs']['HTML'].new_build('devdocs')
#print(docs_build)
#docs_build.stage_all()
print('-'*80)



print('-'*80)
print('- Building package')

print('-'*40)
print('-- Locals')
pkg_conf = project.local_config['./src/conditor']
#pkg_conf['py3pkg'] = True
#pkg_conf['py3pkg.name'] = 'Conditor'
#pkg_conf['py3pkg.package_name'] = 'conditor'
#pkg_conf['py3pkg.package_description'] = 'Hello World'
#pkg_conf['py3pkg.package_version'] = '0'
print(' '+'\n '.join(lce(pkg_conf, True)))
print('-'*40)

print('-- Build')
pkg_build = project.recipe['py3pkg']['Conditor'].new_build('testing')
print(pkg_build)
#pkg_build.stage_all()
print('-'*80)

#import conditor.py3pkg
#print(conditor.py3pkg.find_package_sources(project.path))

#import conditor.__main__
#conditor.__main__.main()

































exit()

def entry_tree(cfgmgr) :
    tree = {}
    for name in cfgmgr.list_entry_names() :
        sector = tree
        for fragment in name.split('.') :
            if fragment not in sector :
                sector[fragment] = {}
                pass
            sector = sector[fragment]
            pass
        pass
    return tree
def listing(cfgmgr, parents=[]) :
    tree = entry_tree(cfgmgr)
    for parent in parents :
        tree = tree[parent]
        pass
    l = []
    for fragment in tree :
        name = '.'.join([*parents, fragment])
        l.append((len(parents), fragment, name))
        if len(tree[fragment]) > 0 :
            l.extend(listing(cfgmgr, values, [*parents, fragment], none_value, undef_value))
            pass
        pass
    return l
vl=listing(gcfg, True)
def pp_listing(value_listing) :
    entry_list = []
    for index in range(len(value_listing)) :
        entry = value_listing[index]
        indent = entry[0]
        tabbing = ''
        if indent >= 1 :
            tabbing += '| ' * (indent-1)
            pass
        name = entry[1]
        value = entry[2]
        entry_list.append(f'{tabbing}+ {name} = {value}')
        if len(value_listing) > index and value_listing[index][0] < entry[0] :
            print('here')
            pass
        pass
    listing_print = '\n'.join(entry_list)
    return listing_print
print(pp_listing(vl))


def test(entry_listing) :
    print_values = True
    value_is = ' = '

    # Composes entry string.
    def compose_entry(index_indent, flow_indent, name, value_str) :
        # Entry name index.
        heading = f'{index_indent}{name}'
        # print value if required.
        if print_values :
            heading += f'{value_is}{value_str}'
            pass
        sl = [heading]
        return sl

    sl = []
    for index in range(len(entry_listing)) :
        entry = entry_listing[index]
        indent = entry[0]
        name = entry[1]
        value = entry[2]
        sl.extend(compose_entry('', '', name, value))
        pass

    return '\n'.join(sl)
print(test(vl))





import conditor.composer.file_tree
#conditor.composer.file_tree.clone_tree_struct('/home/srh/conditor/src', '/home/srh/conditor/testclone')
#def iter_fnc(file) :
#    print(file)
#    return
#conditor.composer.file_tree.iter_tree_func('/home/srh/conditor/src', iter_fnc)

#conditor.composer.file_tree.clone_tree(project.path.joinpath('src'), project.path.joinpath('testdst'))






#next
# actual build tools
# docs
# console
# config type set clear value










exit()
project = conditor.project.find_project(__file__)
print(project.context.format_str('test{print(locals())}'))

exit()
project = conditor.project.find_project(__file__)
docs_build = project.recipe['docs']['Docs'].new_build('testing')
print(docs_build)

#docs_build.stage_run_next()
#docs_build.recipe.run_stage_name(docs_build, 'create')
#i = docs_build.recipe.run_next_stage(docs_build)
#print(i)
docs_build.recipe.run_all_stages(docs_build)


exit()
p = conditor.project.find_project(__file__)
print(p)
print(p.config)

p.config['test'] = 'Hello World2'
print(p.config['test'])

print(p.action['test']['TestAction']())

build = p.builds.new_build('test')
build['test'] = 'hello world'
print(build['test'])

print(p.recipe['docs'].default)
r = p.recipe['docs'].default
print(r.stages)

b = r.new_build('testbuild')
print(b)
print(b.recipe)

