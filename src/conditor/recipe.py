"""Build recipe module."""

import collections.abc
import json
import importlib
import importlib.util
import sys
import pathlib

import conditor
import conditor.compose.sfs

class RecipeManager (collections.abc.Mapping) :
    """Interaction with project recipes."""
    def __init__(self, project) :
        super().__init__()
        self.project = project
        """Reference to project instance."""
        self.entry_cache = {}
        """List of value entries."""
        return
    def __str__(self) :
        return repr(self)
    def __repr__(self) :
        return f'{type(self).__name__}({repr(self.project)})'

    def __getitem__(self, location) :
        return Entry(self, location)
    #def __setitem__(self, name, value) :
    #    # handle value type
    #    # otherwise literal
    #    # if not value object.
    #    entry = Action(self, location)
    #    entry.type = LiteralValue
    #    entry.value.set(value)
    #    return
    #def __delitem__(self, name) :
    #    return
    def __iter__(self) :
        # TODO: import recipes return?
        return iter(self.list_path_entries())
    def __len__(self) :
        return len(self.list_path_entries())

    def list_path_entries(self) :
        """Composes a list of recipe file entries."""
        entries = []
        for node in self.path.iterdir() :
            if node.is_file() and node.suffix == '.py' :
                entries.append(node.with_suffix('').name)
                pass
            pass
        return entries

    @property
    def path(self) :
        """Location of recipe entries directory."""
        return self.project.path.joinpath('./.__conditor_recipes__').resolve()

    def import_recipe(self, location) :
        """Import a recipe from module."""
        __import__(location)
        return ImportEntry(self, location)

    pass

class Entry (collections.abc.Mapping):
    def __new__(cls, recipe_manager, index) :
        if index in recipe_manager.entry_cache :
            return recipe_manager.entry_cache[index]
        entry = super().__new__(cls)
        recipe_manager.entry_cache[index] = entry
        return entry
    def __init__(self, recipe_manager, location) :
        self.recipe_manager = recipe_manager
        """Reference to configuration manager."""
        self.location = location
        """Name of this configuration entry."""
        self.recipe_cache = {}
        self._module = None
        """Initialized value instance of this entry."""
        return
    def __str__(self) :
        return repr(self)
    def __repr__(self) :
        return f'{type(self).__name__}({repr(self.location)})'

    def __getitem__(self, recipe_name) :
        if recipe_name not in self.recipe_cache :
            #self.recipe_cache[recipe_name] = getattr(self.module, recipe_name)(self)
            self.recipe_cache[recipe_name] = getattr(self.module, recipe_name)
            self.recipe_cache[recipe_name].ENTRY = self
            pass
        return self.recipe_cache[recipe_name]
    def __iter__(self) :
        return iter(self.list_class_names())
    def __len__(self) :
        return len(self.list_class_names())

    def list_class_names(self) :
        """Compses a list of recipe class names in this entry module."""
        class_names = []
        for class_name in self.module.__dict__ :
            if type(self.module.__dict__[class_name]) != type :
                continue
            if issubclass(self.module.__dict__[class_name], Recipe) :
                class_names.append(class_name)
                pass
            pass
        return class_names

    @property
    def project(self) :
        return self.recipe_manager.project
    @property
    def relpath(self) :
        # TODO: remove and only use path.
        return f'./{self.location}.py'
    @property
    def path(self) :
        # TODO: handle module import recipes.
        return self.recipe_manager.path.joinpath(self.relpath).resolve()

    @property
    def module_name(self) :
        return f'conditor.recipe.__loaded_recipes__.{self.location}'

    @property
    def module(self) :
        if self._module is None :
            if self.module_name not in sys.modules :
                module_spec = importlib.util.spec_from_file_location(self.module_name, self.path)
                sys.modules[self.module_name] = importlib.util.module_from_spec(module_spec)
                module_spec.loader.exec_module(sys.modules[self.module_name])
                pass
            self._module = sys.modules[self.module_name]
            pass
        return self._module

    @property
    def default(self) :
        """Default recipe class."""
        return self[self.module.DEFAULT_RECIPE]

    pass


class ImportEntry (Entry) :
    @property
    def module_name(self) :
        return self.module.__name__
    @property
    def module(self) :
        __import__(self.location)
        return sys.modules[self.location]
    pass


class Recipe :
    """Recipe superclass."""

    STAGES = []
    """Sequence of stages to be executed when running stage all."""

    NAME = '__UNNAMED__'
    """Name of this recipe."""

    STAGE_PREFIX = 'stage_'
    """Prefix for stage methods."""

    ENTRY = None
    """Reference to entry instance of this recipe after loading."""

    def __init__(self, build=None, flavour={}) :
        #self.entry = entry
        #"""Reference to recipe entry."""
        self._build = build
        self.flavour = flavour
        return
    def __str__(self) :
        return repr(self)
    def __repr__(self) :
        return f'{type(self).__name__}({repr(self.entry.path)})'

    @property
    def build(self) :
        if self._build is None :
            self._build = self.new_build()
        return self._build

    @classmethod
    @property
    def full_name(cls) :
        return (cls.ENTRY.location, cls.__name__)

    @property
    def project(self) :
        """Reference to conditor project."""
        return self.ENTRY.recipe_manager.project

    @classmethod
    def new_build(cls, identity='UNDEFINED_IDENTITY', abs_name=False) :
        """Create a new build using this recipe.

        Args:
            identity:
                Identity used for the build.
            abs_name:
                If the given identity sould be the absolute build instance name.
                Otherwide recipe name will be used as prefix.

        Returns:
            New instance of a build process.
        """
        name = f'{cls.NAME}-{identity}'
        if abs_name :
            name = identity
            pass
        build = cls.ENTRY.project.build.new_build(name, overwrite=True)
        build.recipe = cls
        return build

    def get_stage_name(self, name) :
        """Get stage method by given name."""
        return getattr(self, f'{type(self).STAGE_PREFIX}{name}')

    def run_stage_name(self, name) :
        """Run a stage method by given name."""
        stage = self.get_stage_name(name)
        stage_return = stage()
        #current running flag
        return stage_return

    def run_stage_index(self, index) :
        """Run a stage by given index."""
        stage_name = type(self).STAGES[index]
        return self.run_stage_name(stage_name)

    def run_next_stage(self) :
        """Run next stage or first if not done before."""
        if '__index__' not in self.build :
            self.build['__index__'] = 0
            pass
        if self.build['__index__'] == -1 :
            return -1
        if self.build['__index__'] == len(type(self).STAGES) :
            self.build['__index__'] = -1
            return -1
        self.run_stage_index(self.build['__index__'])
        if self.build['__index__'] + 1 >= len(type(self).STAGES) :
            self.build['__index__'] = -1
            return -1
        self.build['__index__'] = self.build['__index__'] + 1
        return self.build['__index__']

    @classmethod
    def run_all_stages(cls, build, flavour={}) :
        """Run all stages in this recipe."""
        recipe = cls(build, flavour)
        while recipe.run_next_stage() != -1 :
            continue
        return

    pass


class ConditorRecipe (Recipe) :
    """Conditor recipe providing more functionality."""

    PROPERTY_STRUCT = {
        'relpath.src': {
            'desc': 'Relative path in build directory to source tree.',
            'default': './src'
        },
        'relpath.out': {
            'desc': 'Relative path in build directory to output files.',
            'default': './out'
        },
        'path.origin': {
            'desc': 'Absolute path of source tree to clone.'
        },
        'path.dist': {
            'desc': 'Resolved path of output directory.',
            'return': True
        }
    }
    """Dictionary of build property structure.

    Index defines the property name.
    Value is a dictionary defining the followinf entries:
    - desc: Text description of the property
    - default: Default value to be returned as fallback.
    - no_default: If the default value is not to be considered as a value (False if default is defined).
    - return: If True, defines this property represents a return value from the build process (False by default).
    """

    class PropertyManager (collections.abc.Mapping) :
        """Helper to define build properties."""
        def __init__(self, recipe) :
            self.recipe = recipe
            return
        def __contains__(self, name) :
            return self.has(name)
        def __getitem__(self, name) :
            return self.get(name)
        def __iter__(self) :
            return iter(self.compose_list)
        def __len__(self) :
            return len(self.list)
        def compose_list(self) :
            prop_list = [
                *self.compose_recipe_list(),
                *list(self.build),
                *list(self.flavour)
            ]
            return prop_list
        def compose_recipe_list(self, include_no_default=True) :
            """Composes a list of all property names in recipe."""
            prop_list = []
            for prop_name in self.recipe.PROPERTY_STRUCT :
                prop = self.recipe.PROPERTY_STRUCT[prop_name]
                if 'default' in prop :
                    if include_no_default and 'no_default' in prop and prop['no_default'] :
                        continue
                    prop_list[prop_name] = prop['default']
                    pass
                pass
            return prop_list
        @property
        def build(self) :
            return self.recipe.build
        @property
        def flavour(self) :
            return self.recipe.flavour
        @property
        def struct(self) :
            """Structure of build properties.

            TODO:
                Extend with parent class struct.
            """
            return self.recipe.PROPERTY_STRUCT
        def has(self, name, in_recipe=True, in_build=True, in_flavour=True) :
            # If property is in recipe property struct as default.
            if in_recipe and name in self.struct :
                prop = self.struct[name]
                if 'default' in prop :
                    if 'no_default' not in prop or not prop['no_default'] :
                        return True
                    pass
                pass
            if in_build and name in self.build :
                return True
            if in_flavour and name in self.flavour :
                return True
            return False
        def set(self, name, value, overwrite=False) :
            """Set a property value.

            Overwrite is on False for easier initial declarations.
            """
            if not overwrite and self.has(name) :
                return
            self.build[name] = value
            return
        def get(self, name, fallback=None) :
            if self.has(name, in_recipe=False, in_build=False, in_flavour=True) :
                return self.flavour[name]
            if self.has(name, in_recipe=False, in_build=True, in_flavour=False) :
                return self.build[name]
            if self.has(name, in_recipe=True, in_build=False, in_flavour=False) :
                return self.recipe.PROPERTY_STRUCT[name]['default']
            return fallback
        def transfer(self, overwrite=False) :
            """TODO: Use this method to write recipe proeprties to build properties."""
            return
        pass

    def __init__(self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        self._propmgr = None
        """Stores instance of property manager."""
        return

    @property
    def prop(self) :
        if self._propmgr is None :
            self._propmgr = self.PropertyManager(self)
            pass
        return self._propmgr

    @property
    def src_path(self) :
        return self.build.path.joinpath(self.prop['relpath.src']).resolve()
    @property
    def out_path(self) :
        return self.build.path.joinpath(self.prop['relpath.out']).resolve()
    @property
    def origin_path(self) :
        return pathlib.Path(self.prop['path.origin']).resolve()

    def deploy_dir(self) :
        self.src_path.mkdir(parents=True, exist_ok=True)
        self.out_path.mkdir(parents=True, exist_ok=True)
        return

    def clone_source(self, ignore_patterns=[], origin=None, source=None) :
        if origin is None :
            origin = self.origin_path
            pass
        if source is None :
            source = self.src_path
            pass
        conditor.compose.sfs.clone_tree(self.build.context,
            origin, source,
            ignore_patterns=ignore_patterns
        )
        return

    def resolve_dist_path(self, overwrite=False, overwrite_value=None) :
        if overwrite :
            self.build['path.dist'] = overwrite_value
            return overwrite_value
        self.build['path.dist'] = str(self.out_path)
        return str(self.out_path)

    pass







