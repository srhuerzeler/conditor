
import pathlib
import subprocess
import os

import conditor.recipe
import conditor.compose.file_tree


DEFAULT_RECIPE = 'Document'


class Document (conditor.recipe.ConditorRecipe) :
    """Base Sphinx document build recipe."""

    STAGES = ['init', 'configure', 'deploy', 'compose', 'build', 'distribute']
    NAME = 'conditor.sphinx.Document'
    PROPERTY_STRUCT = {**conditor.recipe.ConditorRecipe.PROPERTY_STRUCT,
        'path.sphinx_conf': {
            'desc': 'Path to sphinx configuration file.'
        },
        'sphinx.pythonpath_extends': {
            'desc': 'List of pythonpath extendsions.',
            'default': []
        }
    }

    def stage_init(self) :
        self.prop.set('path.origin', str(self.project.path.joinpath('./docs').resolve()))
        self.prop.set('path.sphinx_conf', str(self.origin_path.joinpath('./conf.py').resolve()))
        return
    def stage_configure(self) :
        return
    def stage_deploy(self) :
        self.deploy_dir()
        return
    def stage_compose(self) :
        self.clone_source()
        # Clone config file.
        conf_dst_path = self.src_path.joinpath('./conf.py').resolve()
        conf_src_path = pathlib.Path(self.prop['path.sphinx_conf']).resolve()
        print(conf_src_path)
        conditor.compose.file_tree.link_file(conf_src_path, conf_dst_path)
        # TODO: Maybe also clone python source tree.
        return
    def stage_build(self) :
        p_cmd = ['sphinx-build', '-a',
            '-b', 'html',
            '-c', self.src_path,
            self.src_path,
            self.out_path
        ]
        # Modify build environment.
        p_env = os.environ.copy()
        pythonpath_list = []
        if 'PYTHONPATH' in p_env :
            pythonpath_list.append(p_env['PYTHONPATH'])
            pass
        pythonpath_list.extend(self.prop['sphinx.pythonpath_extends'])
        p_env['PYTHONPATH'] = os.pathsep.join(pythonpath_list)
        # Execute build.
        p = subprocess.run(p_cmd,
            cwd = self.build.path,
            env = p_env
        )
        return
    def stage_distribute(self) :
        self.resolve_dist_path()
        return

    pass

class HTML (Document) :
    pass


