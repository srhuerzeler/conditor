
import pathlib
import subprocess
import os
import re
import tomlkit

import conditor.recipe
import conditor.compose.file_tree
import conditor.py3pkg.build

class Package (conditor.recipe.ConditorRecipe) :

    STAGES = ['init', 'configure', 'verify', 'deploy', 'compose', 'build', 'distribute']
    NAME = 'conditor.py3pkg.Package'
    PROPERTY_STRUCT = {**conditor.recipe.ConditorRecipe.PROPERTY_STRUCT,
        'py3pkg.codename': {
            'desc': 'Codename of the package. Defaults to project codename.'
        },
        'py3pkg.namespace': {
            'desc': 'Namespace of the python package. Defaults to codename.'
        },
        'py3pkg.version': {
            'desc': 'Version of the package. Defaults to project version or local package config.'
        },
        'py3pkg.authors': {
            'desc': 'List of authors in ("tag", "name", "email") tuple. Defaults to project authors.'
        },
        'py3pkg.scripts': {
            'desc': 'Package executeables.'
        },
        'py3pkg.explicit_packages': {
            'desc': 'Explicit packages to be built.'
        },
        'path.package_build_root': {
            'desc': 'Location of package root directory.',
            'return': True
        }
    }

    def __init__(self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        return

    @property
    def package_codename(self) :
        """Codename of this package."""
        return self.prop.get('py3pkg.codename', self.project.global_config['project.codename'])
    @property
    def package_path(self) :
        """Path of directory which is the referenced package."""
        namespaces = self.package_namespace.split('.')
        return self.src_path.joinpath('/'.join(namespaces)).resolve()
    @property
    def package_namespace(self) :
        """Namespace of this package."""
        return self.prop.get('py3pkg.namespace', self.codename)
    @property
    def package_version(self) :
        """Version of this package."""
        return self.prop.get('py3pkg.version', self.project.global_config['project.codename'])

    def stage_init(self) :
        return
    def stage_configure(self) :
        # Define package source origin.
        self.prop.set('path.origin', str(self.project.path.joinpath(f'./src/{self.project.global_config["project.codename"]}').resolve()))
        # Get package root path and clone locals.
        locals_path = pathlib.Path(self.prop['path.origin']).relative_to(self.project.path)
        locals_mgr = self.project.local_config[locals_path]
        self.clone_locals(locals_mgr)
        return
    def stage_verify(self) :
        # Define required properties.
        self.prop.set('py3pkg.codename', self.project.global_config['project.codename'])
        self.prop.set('py3pkg.namespace', self.prop['py3pkg.codename'])
        self.prop.set('py3pkg.version', self.project.global_config['project.version'])
        self.prop.set('py3pkg.authors', self.project.global_config['project.authors'])
        return
    def stage_deploy(self) :
        source_path = self.build.path.joinpath(self.prop['relpath.src'])
        package_path = source_path.joinpath('/'.join(self.prop['py3pkg.namespace'].split('.'))).resolve()
        resolved_path = str(package_path.relative_to(self.build.path))
        self.build['path.package_build_root'] = str(package_path)
        self.deploy_dir()
        return
    def stage_compose(self) :
        self.clone_source(
            ignore_patterns=['__pycache__'],
            source=pathlib.Path(self.prop['path.package_build_root'])
        )
        ppt = self.compose_pyproject_toml()
        print('-'*80, '\n', tomlkit.dumps(ppt), '\n', '-'*80)
        ppt_str = tomlkit.dumps(ppt)
        ppt_path = self.src_path.joinpath('./pyproject.toml').resolve()
        ppt_path.touch(exist_ok=True)
        ppt_path.write_text(ppt_str)
        sp_path = self.src_path.joinpath('./setup.py').resolve()
        sp_path.touch(exist_ok=True)
        sp_path.write_text('import setuptools\nsetuptools.setup()')
        return
    def stage_build(self) :
        """Executes package build."""
        # Build command.
        p_cmd = ['python3', '-m', 'build',
            '--outdir', self.out_path,
            self.src_path
        ]
        # Build environment.
        p_env = os.environ.copy()
        #p_env['DISTUTILS_DEBUG'] = '1'
        p = subprocess.run(p_cmd,
            cwd = self.build.path,
            env = p_env,
            #shell = True
        )
        print(p)
        # Search distribution files.
        for node in self.out_path.iterdir() :
            if node.match('*.whl') :
                self.build['path.build_dist'] = str(node.resolve())
                continue
            if node.match('*.tar.gz') :
                self.build['path.source_dist'] = str(node.resolve())
                continue
            pass
        return
    def stage_distribute(self) :
        if 'py3pkg.distribute.install' in self.prop and self.prop['py3pkg.distribute.install'] :
            print('INSTALL THIS')
            self.distribute_install()
            pass
        return

    def distribute_install(self) :
        build_dist_path = pathlib.Path(self.prop['path.build_dist'])
        p_cmd = ['python3', '-m', 'pip', 'install',
            '--force-reinstall',
            build_dist_path
        ]
        p_env = os.environ.copy()
        p = subprocess.run(p_cmd,
            cwd = self.build.path,
            env = p_env
        )
        return

    def clone_locals(self, local_config, overwrite_locals=False) :
        """Clones all locals from given local manager to build properties."""
        for config in local_config :
            if re.fullmatch('py3pkg.*', config) :
                if not overwrite_locals and config in self.build :
                    continue
                self.build[config] = local_config[config]
                print('Cloned Local:', config, '=',  local_config[config])
                pass
            pass
        return

    def compose_pyproject_toml(self) :
        ppd = tomlkit.document()
        ppd.add(tomlkit.comment('Composed by `conditor.py3pkg`.'))
        ppd['project'] = tomlkit.table()
        ppd['project']['name'] = self.prop['py3pkg.codename']
        ppd['project']['version'] = self.prop['py3pkg.version']
        ppd['project']['authors'] = tomlkit.array()
        for author in self.prop['py3pkg.authors'] :
            ppd_author = tomlkit.inline_table()
            ppd_author['name'] = author[1]
            ppd_author['email'] = author[2]
            ppd['project']['authors'].add_line(ppd_author)
            pass
        if 'py3pkg.description' in self.prop :
            ppd['project']['description'] = self.prop['py3pkg.description']
            pass
        if 'py3pkg.scripts' in self.prop :
            ppd['project']['scripts'] = tomlkit.table()
            for script in self.prop['py3pkg.scripts'] :
                ppd['project']['scripts'][script] = self.prop['py3pkg.scripts'][script]
                pass
            pass
        ppd['tool'] = tomlkit.table()
        ppd['tool']['setuptools'] = tomlkit.table()
        if 'py3pkg.explicit_packages' in self.prop and len(self.prop['py3pkg.explicit_packages']) > 0:
            ppd['tool']['setuptools']['packages'] = tomlkit.array()
            for explicit_package in self.prop['py3pkg.explicit_packages'] :
                ppd['tool']['setuptools']['packages'].add_line(explicit_package)
                pass
            pass
        return ppd

    pass


class Emulate (Package) :
    """Builds a package source tree for testing without building the entire package."""

    STAGES = ['init', 'configure', 'verify', 'deploy', 'compose', 'distribute']
    NAME = 'conditor.py3pkg.Emulate'
    PROPERTY_STRUCT = {**Package.PROPERTY_STRUCT,
        'path.package_emulate': {
            'desc': 'Path where package source is located.',
            'return': True
        }
    }

    def stage_distribute(self) :
        self.build['path.package_emulate'] = str(self.src_path.resolve())
        return

    pass

