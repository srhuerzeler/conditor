
import subprocess
import pathlib
import os

import conditor.action

def env_extend_pythonpath(*extend, env=None) :
    if env is None :
        env = os.environ.copy()
        pass
    pp_list = []
    if 'PYTHONPATH' in env :
        pp_list.append(env['PYTHONPATH'])
        pass
    pp_list.extend(extend)
    env['PYTHONPATH'] = os.pathsep.join(pp_list)
    return env

def build_emulation(project, package_origin) :
    """Builds the package emulation."""
    project.recipe.import_recipe('conditor.py3pkg.recipe')
    build = project.recipe['conditor.py3pkg.recipe']['Emulate'].new_build()
    pkgo_path = pathlib.Path(package_origin).resolve()
    build.stage_all(flavour={
        'path.origin': str(pkgo_path)
    })
    return build.name

def get_emulation(project, package_build=None, package_origin=None) :
    if package_origin is not None :
        package_build = build_emulation(project, package_origin)
        pass
    if package_build is None :
        return None
    build = project.build[package_build]
    return build['path.package_emulate']


class EmulateScript (conditor.action.Action) :
    """Emulate a python package and execute script with the emulated package in pythonpath."""
    def run(self, package_build=None, package_origin=None, script_path=None) :
        emulation_path = get_emulation(self.project, package_build, package_origin)
        p_cmd = ['python3']
        if script_path is not None :
            p_cmd.append(script_path)
            pass
        p_env = []
        if emulation_path is not None :
            p_env = env_extend_pythonpath(emulation_path)
            pass
        p = subprocess.run(p_cmd,
            env = p_env
        )
        return
    pass


class EmulateScriptREPL (conditor.action.Action) :
    """Emulate a python package and execute script with the emulated package in pythonpath then open REPL."""
    def run(self, package_build=None, package_origin=None, script_path=None) :
        emulation_path = get_emulation(self.project, package_build, package_origin)
        p_cmd = ['python3', '-i']
        if script_path is not None :
            p_cmd.append(script_path)
            pass
        p_env = []
        if emulation_path is not None :
            p_env = env_extend_pythonpath(emulation_path)
            pass
        p = subprocess.run(p_cmd,
            env = p_env
        )
        return
    pass

