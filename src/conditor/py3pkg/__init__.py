
import tomlkit

class File :
    """A Python file."""

    def __init__(self, path) :
        self.path = path
        """Path of file location."""
        return
    def __str__(self) :
        return self.__repr__()
    def __repr__(self) :
        return f'{type(self).__name__}({self.path})'

    pass

class Module (File) :
    """Represents a python module."""

    def __init__(self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        return
    def __str__(self) :
        return self.__repr__()
    def __repr__(self) :
        return f'{type(self).__name__}({self.namespace})'

    @property
    def package(self) :
        """Package this module is part of."""
        if Package.is_package(self.path.parent) :
            return Package(self.path.parent)
        return None

    @property
    def name(self) :
        return self.path.with_suffix('').name

    @property
    def namespace(self) :
        """Namespace of this module."""
        return f'{self.package.namespace}.{self.name}'

    pass


class Package :
    """A Python package."""

    @classmethod
    def is_package(cls, path) :
        if path.joinpath('__init__.py').exists() :
            return True
        return False

    def __init__(self, path) :
        self.path = path
        """Path of package directory."""
        return
    def __str__(self) :
        return self.__repr__()
    def __repr__(self) :
        return f'{type(self).__name__}({self.namespace})'

    @property
    def parent(self) :
        """Parent package"""
        if Package.is_package(self.path.parent) :
            return Package(self.path.parent)
        return None

    @property
    def is_root(self) :
        return self.parent is None

    @property
    def root(self) :
        """Root package."""
        current = self
        while not current.is_root :
            current = current.parent
            pass
        return current

    @property
    def namespace(self) :
        namespace = [self.name]
        current = self
        while not current.is_root :
            current = current.parent
            namespace.append(current.name)
            pass
        namespace.reverse()
        return '.'.join(namespace)

    @property
    def name(self) :
        return self.path.name

    @property
    def modules(self) :
        """List of modules in this package."""
        modules = []
        for node in self.path.iterdir() :
            if node.suffix == '.py' :
                modules.append(Module(node))
                pass
            pass
        return modules

    @property
    def packages(self) :
        """List of sub-packages in this package."""
        packages = []
        for node in self.path.iterdir() :
            if node.is_dir and Package.is_package(node) :
                packages.append(Package(node))
            pass
        return packages

    @property
    def all_packages(self) :
        """List all contained packages."""
        packages = self.packages
        for package in packages :
            packages.extend(package.all_packages)
            pass
        return packages

    pass


