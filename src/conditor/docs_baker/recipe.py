
import pathlib
import subprocess
import os

import conditor.recipe
import conditor.compose.file_tree
import conditor.docs_baker


DEFAULT_RECIPE='Document'


class Document (conditor.recipe.ConditorRecipe) :
    """Composes a Sphinx document from source."""

    STAGES = ['init', 'configure', 'deploy', 'compose', 'build', 'distribute']
    NAME = 'conditor.docs_baker.Document'
    PROPERTY_STRUCT = {**conditor.recipe.ConditorRecipe.PROPERTY_STRUCT,
        'path.dist': {
            'desc': 'Contains a resolved path to the outout directory.',
            'return': True
        },
        'docs_baker.document_name': {
            'desc': 'Name of the output document.',
            'default': '__UNDEFINED_NAME__'
        },
        'docs_baker.document_struct': {
            'desc': 'Custom compose document structure.',
            'default': {}
        }
    }
    def __init__(self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        self._document = None
        """Cache for document instance."""
        self._composer = None
        """Cache for composer instance."""
        return

    @property
    def document(self) :
        if self._document is None :
            self._document = conditor.docs_baker.Document(self.src_path, self.doc_name)
            pass
        return self._document
    @property
    def composer(self) :
        if self._composer is None :
            self._composer = conditor.docs_baker.Composer(self.document, self.out_path)
            self._composer.struct = self.doc_struct
            pass
        return self._composer
    @property
    def doc_name(self) :
        return self.prop['docs_baker.document_name']
    @doc_name.setter
    def doc_name(self, name) :
        self.build['docs_baker.document_name'] = name
        # Reset document cache.
        self._document = None
        return
    @property
    def doc_struct(self) :
        return self.prop['docs_baker.document_struct']

    def stage_init(self) :
        self.prop.set('path.origin', str(self.project.path.joinpath('./docs').resolve()))
        return
    def stage_configure(self) :
        return
    def stage_deploy(self) :
        self.deploy_dir()
        return
    def stage_compose(self) :
        self.clone_source()
        return
    def stage_build(self) :
        self.composer.compose()
        return
    def stage_distribute(self) :
        self.resolve_dist_path()
        return
    pass

