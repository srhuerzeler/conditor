"""Build recipes for the conditor python package."""

import conditor.recipe
import conditor.py3pkg.recipe
import conditor.py3pkg.build

DEFAULT_RECIPE = 'PKG_Conditor'

class EMU_Conditor (conditor.py3pkg.recipe.Emulate) :
    """Emulate the conditor core package."""
    pass

class PKG_Conditor (conditor.py3pkg.recipe.Package) :
    """Build the conditor core package."""
    pass

