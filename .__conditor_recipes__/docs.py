
import conditor.recipe

DEFAULT_RECIPE = 'HTML'

class HTML (conditor.recipe.Recipe) :
    STAGES = ['init', 'configure', 'deploy', 'depends', 'compose', 'build']
    NAME = 'docs.HTML'

    @property
    def bake_build(self) :
        return self.project.build[self.build['bake_build.name']]
    @property
    def sphinx_build(self) :
        return self.project.build[self.build['sphinx_build.name']]

    def stage_init(self) :
        # Import required build recipes.
        self.ENTRY.recipe_manager.import_recipe('conditor.docs_baker.recipe')
        self.ENTRY.recipe_manager.import_recipe('conditor.sphinx.recipe')
        # Create new bake and sphinx build.
        bake_build = self.project.recipe['conditor.docs_baker.recipe'].default.new_build('420')
        self.build['bake_build.name'] = bake_build.name
        sphinx_build = self.project.recipe['conditor.sphinx.recipe']['HTML'].new_build('420')
        self.build['sphinx_build.name'] = sphinx_build.name
        # Document data
        # TODO: defaults or flavour.
        #self.build['docs.project'] = self.project.G['project.title']
        #self.build['docs.author'] = 'Custom Author'
        #self.build['docs.copyright'] = 'cpr'
        return
    def stage_configure(self) :
        self.bake_build['docs_baker.document_name'] = 'conditor'
        # TODO: Implement build.recipe_instance from cache.
        self.sphinx_build['path.include_pythonpath_roots'] = [str(self.project.path.joinpath('./src').resolve())]
        # TODO: implement stage_until for depends.
        return
    def stage_deploy(self) :
        return
    def stage_depends(self) :
        self.depends_bake()
        self.depends_sphinx()
        return
    def depends_bake(self) :
        self.bake_build.stage_all()
        return
    def depends_sphinx(self) :
        self.sphinx_build['path.sphinx_conf'] = str(self.project.path.joinpath('./docs/conf.py').resolve())
        self.sphinx_build['path.origin'] = self.bake_build['path.dist']
        #self.sphinx_build['__propextend_build__'] = self.build.name
        self.sphinx_build.stage_all()
        return
    def stage_compose(self) :
        return
    def stage_build(self) :
        #sphinx_build = self.project.recipe['conditor.sphinx.recipe'].new_build('420')
        return
    pass

